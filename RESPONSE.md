# Response to MAT Fan Engagement Coding Challenge

The component labelled “Your App" in the provided architecture diagram is here
implemented as a Docker Compose service called `processing` which has been
added to the top-level `docker-compose.yaml` file. Source code and supporting
files are contained in the [`response`](response) directory.

The service will start alongside the others with `docker-compose up`.

> __Note:__ Development settings and requirements are simply denoted with eg.
> `# dev` comments for readibility here, but would be more clearly separated
> into dev-specific files for production.

## Missing

In the spirit of getting something working in a few hours, there are quite some
development shortcuts I have taken, the main ones are enumerated below.

 - No test coverage whatsoever, I really like
   [pytest](https://docs.pytest.org/en/latest/) for writing concise and
   readable test cases, and its plugins for CI reporting, etc.
 - No schema checking on MQTT incoming messages, I often use
   [marshmallow](https://marshmallow.readthedocs.io/en/2.x-line/) for things
   like this.
 - The `client.py` script is just a bare Python module. Depending on the
   application I would wrap in a
   [Click](https://click.palletsprojects.com/en/7.x/) to give nice isolation
   for testing, a clean way to pass command line args, etc.
 - Use of global `PREVIOUS` variable to store values. This is bad practice,
   especially in larger applications, and would probably be implemented for
   production with an in-memory datastore such as [Redis](https://redis.io/).
