import datetime
import json
import os

from urllib.parse import urlparse

from geopy.distance import distance as geopy_distance

import paho.mqtt.client as mqtt

PREVIOUS = {}


def calculate_speed(duration, coord_a, coord_b):
    '''
    Return the speed in miles per hour that must have been travelled at to get
    from `coord_a` to `coord_b` in `duration`. 

    Coordinates are long/lat and duration is milliseconds.
    '''
    distance = geopy_distance(
        (coord_a['lat'], coord_a['long']),
        (coord_b['lat'], coord_b['long'])
    )
    return distance.miles / duration * 1000 * 60 * 60


def process_coords(client, _, message):
    'Process incoming long/lat data'
    incoming = json.loads(message.payload.decode('utf-8'))
    index = incoming['carIndex']
    previous = PREVIOUS.get(index)
    if not previous:
        PREVIOUS[index] = incoming
        return
    client.publish('carStatus', json.dumps({
        'timestamp': datetime.datetime.now().timestamp() * 1000,
        'carIndex': index,
        'type': 'SPEED',
        'value': calculate_speed(
            incoming['timestamp'] - previous['timestamp'],
            previous['location'], incoming['location'],
        ),
    }), qos=0)
    PREVIOUS[index] = incoming


broker_addr = urlparse(os.environ['MQTT_URL'])
client = mqtt.Client('processing')
client.on_message = process_coords
client.connect(broker_addr.hostname, port=broker_addr.port)
client.subscribe(os.environ['MQTT_TOPIC'], qos=0)

err = 0

while not err:
    err = client.loop()

print("err: {0}".format(err))
