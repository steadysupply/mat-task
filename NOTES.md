# Notes

 - It would be nice to see sources of images for other components
   https://hub.docker.com/search/?q=mclarenappliedtechnologies&type=image
   to have a clearer overview of the application
 - It would be useful to be able to configure location of backend for the
   `webapp` service, rather than having it assume a backend is available on the
   loopback interface
